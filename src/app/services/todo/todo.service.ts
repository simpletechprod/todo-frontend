import { Injectable } from '@angular/core';
import { TodoList } from 'src/app/models/todo-list.model';
import { TodoItem } from 'src/app/models/todo-item.model';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  endpointURL = environment.baseUrl + '/todos/';

  constructor(
    private httpClient: HttpClient,
    private datePipe: DatePipe
  ) { }

  post(newTodo) {
    newTodo.dueDate = this.datePipe.transform(newTodo.dueDate, 'yyyy-MM-dd');
    return new Observable<TodoItem>((observer) => {
      this.httpClient.post(this.endpointURL, newTodo, {withCredentials: true}).subscribe((result: any) => {
        const todoItem = new TodoItem();
        todoItem.loadFromJSON(result);
        observer.next(todoItem);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  put(newTodo) {
    newTodo.dueDate = this.datePipe.transform(newTodo.dueDate, 'yyyy-MM-dd');
    return new Observable<TodoItem>((observer) => {
      this.httpClient.put(this.endpointURL + newTodo.id + '/', newTodo, {withCredentials: true}).subscribe((result: any) => {
        const todoItem = new TodoItem();
        todoItem.loadFromJSON(result);
        observer.next(todoItem);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  patch(id, newTodoAttributes) {
    return new Observable<TodoItem>((observer) => {
      this.httpClient.patch(this.endpointURL + id + '/', newTodoAttributes, {withCredentials: true}).subscribe((result: any) => {
        const todoItem = new TodoItem();
        todoItem.loadFromJSON(result);
        observer.next(todoItem);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  delete(todoItem) {
    return new Observable<boolean>((observer) => {
      this.httpClient.delete(this.endpointURL + todoItem.id + '/', {withCredentials: true}).subscribe((result: any) => {
        observer.next(true);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

}
