import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewTodoInputComponent } from './new-todo-input.component';

describe('NewTodoInputComponent', () => {
  let component: NewTodoInputComponent;
  let fixture: ComponentFixture<NewTodoInputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTodoInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTodoInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
