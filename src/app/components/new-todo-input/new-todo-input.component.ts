import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TodoList } from 'src/app/models/todo-list.model';
import { TodoService } from 'src/app/services/todo/todo.service';
import {TodoItem} from '../../models/todo-item.model';

@Component({
  selector: 'app-new-todo-input',
  templateUrl: './new-todo-input.component.html',
  styleUrls: ['./new-todo-input.component.css']
})
export class NewTodoInputComponent implements OnInit {

  @Output() itemAdded = new EventEmitter();
  @Input() todoList: TodoList;

  newItemForm: FormGroup;

  constructor(
    private todoService: TodoService
  ) { }

  ngOnInit(): void {
    this.newItemForm = new FormGroup({
      itemName: new FormControl(null, Validators.required)
    });
  }

  addItem($event, formDirective) {
    if (this.newItemForm.valid) {
      $event.target.blur();
      this.todoService.post(new TodoItem(this.todoList.id, this.newItemForm.value.itemName)).subscribe(result => {
        formDirective.resetForm();
        this.newItemForm.reset();
        this.itemAdded.emit();
      });
    }
  }

}
