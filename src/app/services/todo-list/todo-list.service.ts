import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {TodoList} from '../../models/todo-list.model';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  endpointURL = environment.baseUrl + '/todo-lists/';

  constructor(
    private httpClient: HttpClient
  ) { }

  getAll() {
    return new Observable<TodoList[]>((observer) => {
      this.httpClient.get(this.endpointURL, {withCredentials: true}).subscribe((result: any) => {
        const todoLists = [];
        for (const jsonTodoList of result) {
          const todoList = new TodoList();
          todoList.loadFromJSON(jsonTodoList);
          todoLists.push(todoList);
        }
        observer.next(todoLists);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  get(id) {
    return new Observable<TodoList>((observer) => {
      this.httpClient.get(this.endpointURL + id + '/', {withCredentials: true}).subscribe((result: any) => {
        const todoList = new TodoList();
        todoList.loadFromJSON(result);
        observer.next(todoList);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

  post(newTodoList) {
    return new Observable((observer) => {
      this.httpClient.post(this.endpointURL, newTodoList, {withCredentials: true}).subscribe((result: any) => {
        const todoList = new TodoList();
        todoList.loadFromJSON(result);
        observer.next(todoList);
        observer.complete();
      }, error => {
        observer.error(error);
        observer.complete();
      });
    });
  }

}
