import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DialogInformationComponent} from '../dialog-information/dialog-information.component';
import {DialogQuestionComponent} from '../dialog-question/dialog-question.component';
import {TranslateService} from '@ngx-translate/core';
import {TodoService} from '../../services/todo/todo.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {

  @Input() listItem;
  @Output() itemDeleted = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    private translate: TranslateService,
    private todoService: TodoService
  ) { }

  ngOnInit(): void {
  }

  toggleFavourite() {
    this.listItem.favourite = !this.listItem.favourite;
    this.todoService.patch(this.listItem.id, {favourite: this.listItem.favourite}).subscribe(result => {
      this.listItem = result;
    });
  }

  openDialog() {
    this.translate.get('Popup_text', {item_name: this.listItem.title}).subscribe(popupText => {
      const myDialog = this.dialog.open(DialogQuestionComponent, {
        disableClose: true,
        data: {
          title: 'My Title',
          text: popupText,
          labelNo: 'Finalement non merci!',
          labelYes: 'Vas-y!'
        }
      });
      myDialog.afterClosed().subscribe(result => {
        console.log(result);
      });

    });
  }

  toggleCompleted() {
    this.todoService.put(this.listItem).subscribe(result => {
      this.listItem = result;
    });
  }

  deleteTodo() {
    this.todoService.delete(this.listItem).subscribe(result => {
      this.itemDeleted.emit();
    });
  }

}
