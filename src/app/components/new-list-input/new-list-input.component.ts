import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TodoService } from 'src/app/services/todo/todo.service';
import {TodoListService} from '../../services/todo-list/todo-list.service';
import {TodoList} from '../../models/todo-list.model';

@Component({
  selector: 'app-new-list-input',
  templateUrl: './new-list-input.component.html',
  styleUrls: ['./new-list-input.component.css']
})
export class NewListInputComponent implements OnInit {

  @Output() itemAdded = new EventEmitter<null>();
  newListForm: FormGroup;

  constructor(
    private todoListService: TodoListService
  ) { }

  ngOnInit(): void {
    this.newListForm = new FormGroup({
      listName: new FormControl(null, Validators.required)
    });
  }

  addList($event, formDirective) {
    if (this.newListForm.valid) {
      $event.target.blur();
      this.todoListService.post(new TodoList(this.newListForm.value.listName)).subscribe(result => {
        console.log(result);
        formDirective.resetForm();
        this.newListForm.reset();
        this.itemAdded.emit();
      });
    }
  }

}
