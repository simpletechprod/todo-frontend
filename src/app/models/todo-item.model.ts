export class TodoItem {
    id = Date.now() + Math.round(Math.random() * 1000);
    title = '';
    dueDate = new Date();
    completed = false;
    favourite = false;
    list = -1;

    constructor(list = -1, title = '') {
      this.list = list;
      this.title = title;
    }

    loadFromJSON(jsonElement) {
      Object.assign(this, jsonElement);
    }
}
