import { TodoItem } from './todo-item.model';

export class TodoList {
    id = Date.now() + Math.round(Math.random() * 1000);
    name = '';
    todos: TodoItem[] = [];

    constructor(name = '') {
        this.name = name;
    }

    loadFromJSON(jsonElement) {
      Object.assign(this, jsonElement);

      this.todos = [];
      if (jsonElement.todos !== undefined) {
        for (const todoJson of jsonElement.todos) {
          const todoItem = new TodoItem();
          todoItem.loadFromJSON(todoJson);
          this.todos.push(todoItem);
        }
      }
    }
}
