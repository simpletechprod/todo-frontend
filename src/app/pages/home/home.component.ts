import { Component, OnInit } from '@angular/core';
import { TodoList } from 'src/app/models/todo-list.model';
import { TodoService } from 'src/app/services/todo/todo.service';
import {SessionLoginService} from '../../services/session-login/session-login.service';
import {Router} from '@angular/router';
import {TodoListService} from '../../services/todo-list/todo-list.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  todoListsArray: TodoList[];
  selectedTodoList: TodoList;

  constructor(
    private todoService: TodoService,
    private router: Router,
    private loginService: SessionLoginService,
    private todoListService: TodoListService
  ) { }

  ngOnInit(): void {
    this.todoListService.getAll().subscribe(todoLists => {
      this.todoListsArray = todoLists;
      if (this.todoListsArray.length > 0) {
        this.todoListService.get(this.todoListsArray[0].id).subscribe(todoList => {
          this.selectedTodoList = todoList;
        });
      }
    });
  }

  selectTodoList(pTodoList) {
    this.todoListService.get(pTodoList.id).subscribe(todoList => {
      this.selectedTodoList = todoList;
    });
  }

  onListAdded() {
    this.todoListService.getAll().subscribe(todoLists => {
      this.todoListsArray = todoLists;
    });
  }

  onItemAdded() {
    this.todoListService.get(this.selectedTodoList.id).subscribe(todoList => {
      this.selectedTodoList = todoList;
    });
  }

  onItemDeleted() {
    this.todoListService.get(this.selectedTodoList.id).subscribe(todoList => {
      this.selectedTodoList = todoList;
    });
  }

  logout() {
    this.loginService.logout().subscribe(result => {
      this.router.navigate(['/login']);
    });
  }

}
