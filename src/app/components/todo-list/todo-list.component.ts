import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  @Input() todoList;
  @Output() itemDeleted = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  getFavouriteTasks(isFavourite) {
    return this.todoList.filter(item => item.favourite === isFavourite);
  }

  onItemDeleted() {
    this.itemDeleted.emit();
  }

}
